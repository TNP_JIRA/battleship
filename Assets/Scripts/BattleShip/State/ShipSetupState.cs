﻿using BattleShip.Manager;

namespace BattleShip.State
{
    public class ShipSetupState : GameState
    {
        private Team _team;

        public ShipSetupState(GameManager gameManager) : base(gameManager)
        {
        }

        protected override void Enter()
        {
            base.Enter();

            _team = GameManager.GetNextTeam();
            if (_team.IsShipSetup)
            {
                GameManager.StartGame();
                return;
            }

            GameManager.Instance.SetTeamEditMode(_team, true);
        }

        protected override void Update()
        {
            base.Update();

            if (_team.IsShipSetup)
                SetNextState(new ShipSetupState(GameManager));
        }

        protected override void Exit()
        {
            base.Exit();
            GameManager.Instance.SetTeamEditMode(_team, false);
        }
    }
}