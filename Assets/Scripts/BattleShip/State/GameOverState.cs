﻿using BattleShip.Manager;

namespace BattleShip.State
{
    public class GameOverState : GameState
    {
        public GameOverState(GameManager gameManager) : base(gameManager)
        {
        }
    }
}