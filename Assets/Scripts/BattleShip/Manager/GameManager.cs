﻿using System;
using BattleShip.Factory;
using BattleShip.ScriptableObjects;
using BattleShip.Ship;
using BattleShip.State;
using BattleShip.System;
using Singleton;
using UnityEngine;

namespace BattleShip.Manager
{
    public class GameManager : MonoSingleton<GameManager>
    {
        [Header("GAME")] [SerializeField] private GameConfig gameConfig;
        [Header("TEAM")] [SerializeField] private TeamData teamData;
        [Header("REGION")] [SerializeField] private Region teamARegion;
        [SerializeField] private Region teamBRegion;

        public override void Awake()
        {
            base.Awake();
            Debug.Assert(teamARegion != null, "teamARegion can't be null!");
            Debug.Assert(teamBRegion != null, "teamBRegion can't be null!");
        }

        [Serializable]
        public struct TeamData
        {
            public string teamAName;
            public string teamBName;
            public ShipFactory.ShipType[] ships;
        }

        public struct GameData
        {
            public Team Winner;
        }

        public enum State
        {
            Waiting,
            Setup,
            Started,
            End
        }

        public bool IsGameOver => _state == State.End;
        public bool IsTeamATurn { get; private set; }
        public Team TeamA { get; private set; }
        public Team TeamB { get; private set; }
        public Team ActiveTeam => IsTeamATurn ? TeamA : TeamB;

        public GameConfig GameConfig => gameConfig;
        public event Action<Team, ObjectEditorManager.EditMode> OnShipSetupModeChanged;
        public event Action OnPreStarted;
        public event Action OnSetup;
        public event Action OnStarted;
        public event Action<Team> OnNextTurn;
        public event Action<Team> OnEndTurn;
        public event Action<GameData> OnGameOver;
        public event Action<Bullet> OnShipFired;
        public event Action<ShipBase> OnShipDamage;
        public event Action<ShipBase> OnShipDestroyed;
        public event Action<int> OnShipAmmoChanged;
        public event Action OnButtonClick;

        private GameState _gameState;
        private State _state;

        private void Start()
        {
            PreStarted();
        }

        private void PreStarted()
        {
            _state = State.Waiting;
            SpawnTeam();
            OnPreStarted?.Invoke();
        }

        private void SpawnTeam()
        {
            TeamA = new Team(teamData.teamAName, BuildTeam(teamARegion.SpawnPoint), teamARegion);
            TeamB = new Team(teamData.teamBName, BuildTeam(teamBRegion.SpawnPoint), teamBRegion);
        }

        private void Update()
        {
            _gameState = _gameState?.Process();
        }

        public void Setup()
        {
            if (_state != State.Waiting) return;
            _state = State.Setup;
            _gameState = new ShipSetupState(this);
            OnSetup?.Invoke();
        }

        public void StartGame()
        {
            _gameState.SetNextState(new NextTurnState(this));
            OnStarted?.Invoke();
        }

        public Team GetNextTeam()
        {
            IsTeamATurn = !IsTeamATurn;
            var team = ActiveTeam;
            var ship = team.GetRandomMember();
            team.SetActiveMember(ship);
            return team;
        }

        public Team NextTurn()
        {
            var team = GetNextTeam();
            OnNextTurn?.Invoke(team);
            return team;
        }

        public void EndTurn()
        {
            OnEndTurn?.Invoke(ActiveTeam);
        }

        private Team GetWinnerTeam()
        {
            if (TeamA.Alive == 0)
                return TeamB;
            return TeamB.Alive == 0 ? TeamA : null;
        }

        private void CheckGameOver()
        {
            var winnerTeam = GetWinnerTeam();
            if (winnerTeam == null) return;

            _gameState.SetNextState(new GameOverState(this));
            _state = State.End;
            OnGameOver?.Invoke(new GameData()
            {
                Winner = winnerTeam,
            });
        }

        public void Restart()
        {
            SceneLoader.LoadScene(SceneLoader.SceneType.Game);
        }

        public void MainMenu()
        {
            SceneLoader.LoadScene(SceneLoader.SceneType.MainMenu);
        }

        private ShipBase[] BuildTeam(Transform spawnPoint)
        {
            var team = ShipFactory.Instance.Create(teamData.ships);
            var padding = Vector3.zero;
            foreach (var ship in team)
            {
                var shipTransform = ship.transform;
                shipTransform.position = spawnPoint.position + padding;
                shipTransform.rotation = spawnPoint.rotation;
                padding += new Vector3(2f, 0f, 0f);

                ship.OnShipDestroyedEvent += OnShipDestroyedEvent;
                ship.OnHealthChanged += (health) => { OnShipDamageEvent(ship); };
                ship.Cannon.OnFire += OnShootEvent;
                ship.Cannon.OnAmmoChanged += OnShipAmmoChangedEvent;
            }

            return team;
        }

        private void OnShipDestroyedEvent(ShipBase ship)
        {
            ship.OnShipDestroyedEvent -= OnShipDestroyedEvent;
            ship.Cannon.OnFire -= OnShootEvent;
            ship.Cannon.OnAmmoChanged -= OnShipAmmoChangedEvent;

            OnShipDestroyed?.Invoke(ship);
            CheckGameOver();
        }

        private void OnShootEvent(Bullet bullet)
        {
            OnShipFired?.Invoke(bullet);
        }

        private void OnShipAmmoChangedEvent(int ammo)
        {
            OnShipAmmoChanged?.Invoke(ammo);
        }

        private void OnShipDamageEvent(ShipBase ship)
        {
            OnShipDamage?.Invoke(ship);
        }

        public void ButtonClickEvent()
        {
            OnButtonClick?.Invoke();
        }

        public void SetTeamEditMode(Team team, bool active)
        {
            var setupMode = active ? ObjectEditorManager.EditMode.On : ObjectEditorManager.EditMode.Off;
            foreach (var ship in team.Members)
            {
                ship.GetComponent<ObjectEditorManager>()
                    .SetEditMode(setupMode);

                ship.GetComponent<ObjectEditor>()
                    .SetLayerMask(team.TeamRegion.RegionLayerMask);
            }

            OnShipSetupModeChanged?.Invoke(team, setupMode);
        }
    }
}