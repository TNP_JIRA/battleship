﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BattleShip.Manager
{
    public class SceneLoader : MonoBehaviour
    {
        [SerializeField] private SceneType sceneType;

        [Serializable]
        public enum SceneType
        {
            Intro = 0,
            MainMenu = 1,
            Game = 2,
        }

        public static void LoadScene(SceneType sceneType)
        {
            SceneManager.LoadScene((int) sceneType);
        }

        public void Load()
        {
            LoadScene(sceneType);
        }
    }
}