﻿using UnityEngine;
using Utils;

namespace BattleShip.Manager
{
    public class MainMenuManager : MonoBehaviour
    {
        public void Play()
        {
            SoundManager.Play(SoundType.Button);
            FadeScreen.FadeOut(1f).setOnComplete(() => { SceneLoader.LoadScene(SceneLoader.SceneType.Game); });
        }

        public void Quit()
        {
            SoundManager.Play(SoundType.Button);
            Application.Quit();
        }
    }
}