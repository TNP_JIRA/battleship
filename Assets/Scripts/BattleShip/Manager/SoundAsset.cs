﻿using System;
using Singleton;
using UnityEngine;

namespace BattleShip.Manager
{
    [Serializable]
    public enum SoundType
    {
        Button,
        Warning
    }

    public class SoundAsset : ResourceSingleton<SoundAsset>
    {
        [SerializeField] private AudioClip button;
        [SerializeField] private AudioClip warning;

        public AudioClip GetSound(SoundType soundType)
        {
            AudioClip audioClip = null;
            switch (soundType)
            {
                case SoundType.Button:
                    audioClip = button;
                    break;
                case SoundType.Warning:
                    audioClip = warning;
                    break;
            }

            return audioClip;
        }
    }
}