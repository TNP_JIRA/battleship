﻿using BattleShip.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.UI
{
    public class ShipSetupUI : BaseUI
    {
        [SerializeField] private TextMeshProUGUI teamText;
        [SerializeField] private Button finishButton;

        private void Awake()
        {
            Debug.Assert(teamText != null, "teamText can't be null!");
            Debug.Assert(finishButton != null, "finishButton can't be null!");
        }

        public void SetTeam(Team team)
        {
            finishButton.onClick.RemoveAllListeners();
            finishButton.onClick.AddListener(() =>
            {
                team.SetFinishSetup();
                GameManager.Instance.ButtonClickEvent();
            });
            teamText.SetText($"{team.TeamName} IS SETTING UP SHIP");
        }
    }
}