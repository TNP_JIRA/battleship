﻿using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.UI
{
    public class BarUI : BaseUI
    {
        [SerializeField] private Image barImage;

        private void Awake()
        {
            Debug.Assert(barImage != null, "barImage can't be null!");
        }

        public void SetFillAmount(float value)
        {
            barImage.fillAmount = value;
        }
    }
}