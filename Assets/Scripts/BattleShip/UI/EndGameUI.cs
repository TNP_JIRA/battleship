﻿using BattleShip.Manager;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.UI
{
    public class EndGameUI : BaseUI
    {
        [SerializeField] private TextMeshProUGUI winnerText;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button mainMenuButton;

        private void Awake()
        {
            restartButton.onClick.AddListener(() =>
            {
                GameManager.Instance.Restart();
                GameManager.Instance.ButtonClickEvent();
            });
            mainMenuButton.onClick.AddListener(() =>
            {
                GameManager.Instance.MainMenu();
                GameManager.Instance.ButtonClickEvent();
            });
        }

        public void SetWinner(Team winner)
        {
            winnerText.SetText($"{winner.TeamName} WIN THE GAME!");
        }
    }
}