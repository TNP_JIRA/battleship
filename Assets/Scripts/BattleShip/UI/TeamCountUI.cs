﻿using BattleShip.Manager;
using TMPro;
using UnityEngine;

namespace BattleShip.UI
{
    public class TeamCountUI : BaseUI
    {
        [SerializeField] private TextMeshProUGUI teamACountText;
        [SerializeField] private TextMeshProUGUI teamBCountText;

        private void Awake()
        {
            Debug.Assert(teamACountText != null, "teamACountText can't be null!");
            Debug.Assert(teamBCountText != null, "teamBCountText can't be null!");
        }

        public void SetTeamCount(Team teamA, Team teamB)
        {
            teamACountText.SetText($"{teamB.TeamName} {teamB.Alive}");
            teamBCountText.SetText($"{teamA.Alive} {teamA.TeamName}");
        }
    }
}