﻿using UnityEngine;

namespace BattleShip.Ship.ScriptableObjects
{
    [CreateAssetMenu(fileName = "New Ship Config", menuName = "Ship Config")]
    public class ShipConfig : ScriptableObject
    {
        [SerializeField] private float health;
        [SerializeField] private float attack;
        [SerializeField] private int ammo;
        [SerializeField] private float fuel;
        public float Health => health;
        public float Attack => attack;
        public int Ammo => ammo;
        public float Fuel => fuel;
    }
}