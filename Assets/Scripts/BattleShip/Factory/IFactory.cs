﻿namespace BattleShip.Factory
{
    public interface IFactory<T>
    {
        T Create();
    }
}