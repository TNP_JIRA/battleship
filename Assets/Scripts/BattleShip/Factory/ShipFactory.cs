﻿using System;
using BattleShip.Ship;
using Singleton;
using UnityEngine;
using Utils;

namespace BattleShip.Factory
{
    public class ShipFactory : MonoSingleton<ShipFactory>
    {
        [SerializeField] private ShipBase smallShipPrefab;
        [SerializeField] private ShipBase mediumShipPrefab;
        [SerializeField] private ShipBase largeShipPrefab;

        public enum ShipType
        {
            Small,
            Medium,
            Large,
        }

        public ShipBase Create(ShipType shipType)
        {
            return shipType switch
            {
                ShipType.Small => Instantiate(smallShipPrefab),
                ShipType.Medium => Instantiate(mediumShipPrefab),
                ShipType.Large => Instantiate(largeShipPrefab),
                _ => throw new ArgumentOutOfRangeException(nameof(shipType), shipType, null)
            };
        }

        public ShipBase[] Create(ShipType[] shipTypes)
        {
            var ships = new ShipBase[shipTypes.Length];
            for (var i = 0; i < shipTypes.Length; i++)
            {
                var shipType = shipTypes[i];
                var ship = Create(shipType);
                ships[i] = ship;
            }

            return ships;
        }
    }
}