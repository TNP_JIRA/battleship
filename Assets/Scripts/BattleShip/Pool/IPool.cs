﻿namespace BattleShip.Pool
{
    public interface IPool<T>
    {
        void InitWarm(int amount);
        T Request();
        void Return(T effect);
    }
}