﻿using BattleShip.Manager;
using BattleShip.Pool;
using BattleShip.Ship;
using UnityEngine;

namespace BattleShip.Listener
{
    public class EffectListener : BaseListener
    {
        [SerializeField] private EffectPool fireEffect;
        [SerializeField] private EffectPool explodeEffect;

        protected override void Initialize()
        {
            GameManager.Instance.OnShipFired += OnShipFiredEvent;
            GameManager.Instance.OnShipDestroyed += OnShipDestroyedEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnShipFired -= OnShipFiredEvent;
            GameManager.Instance.OnShipDestroyed -= OnShipDestroyedEvent;
        }

        private void OnShipFiredEvent(Bullet bullet)
        {
            var effect = fireEffect.Request();
            effect.transform.position = bullet.transform.position;
            effect.transform.rotation = GameManager.Instance.ActiveTeam.ActivePlayer.Cannon.transform.rotation;
            effect.gameObject.SetActive(false);
            effect.gameObject.SetActive(true);
        }

        private void OnShipDestroyedEvent(ShipBase ship)
        {
            var effect = explodeEffect.Request();
            effect.transform.position = ship.transform.position;
            effect.gameObject.SetActive(false);
            effect.gameObject.SetActive(true);
        }
    }
}