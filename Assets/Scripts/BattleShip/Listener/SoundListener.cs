﻿using BattleShip.Manager;
using BattleShip.Ship;
using UnityEngine;

namespace BattleShip.Listener
{
    public class SoundListener : BaseListener
    {
        [SerializeField] private AudioClip victory;
        [SerializeField] private AudioClip nextTurn;
        [SerializeField] private AudioClip shipDamage;
        [SerializeField] private AudioClip shipDestroyed;
        [SerializeField] private AudioClip shipFired;

        [Header("Background Loop")] [Range(0, 1f)] [SerializeField]
        private float seaAmbienceVolume = 1f;

        [SerializeField] private AudioClip seaAmbience;

        [Space] [Range(0, 1f)] [SerializeField]
        private float battleAmbienceVolume = 1f;

        [SerializeField] private AudioClip battleAmbience;

        private void Awake()
        {
            Debug.Assert(victory != null, "victory can't be null!");
            Debug.Assert(nextTurn != null, "nextTurn can't be null!");
            Debug.Assert(shipDamage != null, "shipDamage can't be null!");
            Debug.Assert(shipDestroyed != null, "shipDestroyed can't be null!");
            Debug.Assert(shipFired != null, "shipFired can't be null!");

            Debug.Assert(seaAmbience != null, "seaAmbience can't be null!");
            Debug.Assert(battleAmbience != null, "battleAmbience can't be null!");
        }

        protected override void Initialize()
        {
            GameManager.Instance.OnPreStarted += OnPreStartedEvent;
            GameManager.Instance.OnStarted += OnStartedEvent;
            GameManager.Instance.OnGameOver += OnGameOverEvent;

            GameManager.Instance.OnShipSetupModeChanged += OnShipSetupModeChangedEvent;
            GameManager.Instance.OnNextTurn += OnNextTurnEvent;
            GameManager.Instance.OnButtonClick += OnButtonClickEvent;
            GameManager.Instance.OnShipDamage += OnShipDamageEvent;
            GameManager.Instance.OnShipDestroyed += OnShipDestroyedEvent;
            GameManager.Instance.OnShipFired += OnShipFiredEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnPreStarted -= OnPreStartedEvent;
            GameManager.Instance.OnStarted -= OnStartedEvent;
            GameManager.Instance.OnGameOver -= OnGameOverEvent;

            GameManager.Instance.OnShipSetupModeChanged -= OnShipSetupModeChangedEvent;
            GameManager.Instance.OnNextTurn -= OnNextTurnEvent;
            GameManager.Instance.OnButtonClick -= OnButtonClickEvent;
            GameManager.Instance.OnShipDamage -= OnShipDamageEvent;
            GameManager.Instance.OnShipDestroyed -= OnShipDestroyedEvent;
            GameManager.Instance.OnShipFired -= OnShipFiredEvent;
        }


        private AudioSource _seaAudioSource;
        private AudioSource _battleAudioSource;

        private void OnPreStartedEvent()
        {
            _seaAudioSource = SoundManager.Play(seaAmbience);
            _seaAudioSource.loop = true;
            _seaAudioSource.volume = seaAmbienceVolume;
        }

        private void OnStartedEvent()
        {
            _seaAudioSource.Stop();
            _battleAudioSource = SoundManager.Play(battleAmbience);
            _battleAudioSource.loop = true;
            _battleAudioSource.volume = battleAmbienceVolume;
        }

        private void OnGameOverEvent(GameManager.GameData obj)
        {
            _battleAudioSource.Stop();
            SoundManager.Play(victory);
        }

        private void OnShipSetupModeChangedEvent(Team arg1, ObjectEditorManager.EditMode editMode)
        {
            if (editMode == ObjectEditorManager.EditMode.On)
                SoundManager.Play(SoundType.Warning);
        }

        private void OnButtonClickEvent()
        {
            SoundManager.Play(SoundType.Button);
        }

        private void OnNextTurnEvent(Team team)
        {
            SoundManager.Play(nextTurn);
        }

        private void OnShipDamageEvent(ShipBase ship)
        {
            SoundManager.Play(shipDamage);
        }

        private void OnShipDestroyedEvent(ShipBase ship)
        {
            SoundManager.Play(shipDestroyed).spatialBlend = 0.9f;
        }

        private void OnShipFiredEvent(Bullet bullet)
        {
            SoundManager.Play(shipFired, bullet.transform.position);
        }
    }
}