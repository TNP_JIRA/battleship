﻿using System;
using BattleShip.Manager;
using BattleShip.UI;
using UnityEngine;

namespace BattleShip.Listener
{
    public class AmmoListener : BaseListener
    {
        [SerializeField] private ItemBarUI ammoBar;

        private void Awake()
        {
            Debug.Assert(ammoBar != null, "ammoBar can't be null!");
        }

        protected override void Initialize()
        {
            GameManager.Instance.OnNextTurn += OnNextTurnEvent;
            GameManager.Instance.OnShipAmmoChanged += OnShipAmmoChangedEvent;
            GameManager.Instance.OnGameOver += OnGameOverEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnNextTurn -= OnNextTurnEvent;
            GameManager.Instance.OnShipAmmoChanged -= OnShipAmmoChangedEvent;
            GameManager.Instance.OnGameOver -= OnGameOverEvent;
        }


        private void OnNextTurnEvent(Team team)
        {
            ammoBar.Show();
        }

        private void OnShipAmmoChangedEvent(int ammo)
        {
            ammoBar.SetItemAmount(ammo);
        }

        private void OnGameOverEvent(GameManager.GameData obj)
        {
            ammoBar.Hide();
        }
    }
}